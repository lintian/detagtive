Detagtive - Friendly Packaging Advice on the Web
================================================

Here you will find the code for a web site that shows Lintian tags for
each package in the Debian archive. We also offer tag definitions and
some statistics you may find helpful.

The website is customarily found at
[lintian.debian.org](https://lintian.debian.org/).

Broader Goals
=============

The purpose of the web site is to offer a graphical environment in
which you can explore Lintian tags. You should be able to see all the
tags in your Debian packages without running Lintian yourself. Of
course, it works only for packages in the archive.

We strive to provide some simple statistics for easy comparison. For
example, you can see other packages that trigger the same tag.

As a longer-rage goal, we also hope to provide more detailed
statistics on how effective Lintian's tags are. Some tags have been
overridden thousands of times. Perhaps they should be removed.

Find What You Need
==================

You probably came to this web site for a variety of reasons. Perhaps
you are looking for packaging advice from your fellow maintainers;
perhaps you would like to file a bug against Lintian.

Either way, we hope that you find here what you need.

Spin up Your Own
================
Run your own website with this command (in fish):

    env 'DEBUG=detagtive:*' 'HOME=../../config' npm start

Questions or Problems?
======================

Please contact the [Lintian
Maintainers](https://qa.debian.org/developer.php?email=lintian-maint%40debian.org)
for additional needs. We look forward to your feedback. You are
helping the entire community.

Contributions to the
[`lintian`](https://tracker.debian.org/pkg/lintian) executable should
please be filed over there.
