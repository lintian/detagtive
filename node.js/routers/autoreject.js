// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

module.exports = router;

const app = require('../server')
const database = app.database

const get_latest_lintian = `
SELECT
    MAX(lintian_version) AS max
FROM
    lintian.tags
`

router.get('/', async (req, res, next) => {

    const get_tags = `
WITH lintian AS (
    SELECT
        MAX(lintian_version) AS latest
    FROM
        lintian.tags
),
known_names AS (
    SELECT
        t.tag_name AS apparent_name,
        t.tag_name,
        FALSE AS renamed
    FROM
        lintian.tags AS t
    WHERE
        t.lintian_version = (
            SELECT
                latest
            FROM
                lintian)
        UNION
        SELECT
            rt.renamed_from AS apparent_name,
            rt.tag_name,
            TRUE AS renamed
        FROM
            lintian.renamed_tags AS rt
        WHERE
            rt.lintian_version = (
                SELECT
                    latest
                FROM
                    lintian))
    SELECT
        ar.mandatory,
        ar.tag_name AS apparent_name,
        k1.tag_name,
        k1.renamed,
        t.visibility,
        (
            SELECT
                latest
            FROM
                lintian) AS lintian_version
    FROM
        archive.autoreject_tags AS ar
    LEFT JOIN known_names AS k1 ON k1.apparent_name = ar.tag_name
    LEFT JOIN lintian.tags AS t ON t.tag_name = k1.tag_name
        AND t.lintian_version = (
            SELECT
                latest
            FROM
                lintian)
    ORDER BY
        mandatory DESC,
        apparent_name
`

    try {
        const dbres_tags = await database.query(get_tags)

        let mandatory = [];
        let overridable = [];

        for (const t of dbres_tags.rows) {

            t.label = t.apparent_name
            t.url = '/tags/' + t.tag_name

            if (t.mandatory)
                mandatory.push(t)
            else
                overridable.push(t)
        }

        res.render('autoreject', {
            mandatory: mandatory,
            overridable: overridable
        })

    } catch (err) {
        res.send(err.stack)
    }
})
