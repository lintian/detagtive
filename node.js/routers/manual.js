// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

module.exports = router;

const app = require('../server')
const database = app.database

router.get('/', async (req, res, next) => {

    res.redirect(302, '/manual/index.html')
})

router.get('/:path', async (req, res) => {

    const path = req.params.path

    const get_manual_page_query = `
SELECT
    html
FROM
    lintian.manual_pages
WHERE
    path = $1
ORDER BY
    lintian_version DESC
LIMIT 1
`

    try {
        const dbres = await database.query(get_manual_page_query, [path])

        if (!dbres.rows.length) {

            const known_page = 'index.html'
            if (path != known_page) {
                res.redirect(302, '/manual/' + known_page)
                return
            }

            throw new Error("Manual page unavailable for " + path)
        }

        https://stackoverflow.com/a/44882366
        res.setHeader('Content-Type', 'text/html')
        res.send(dbres.rows[0].html)

    } catch (err) {
        res.send(err.stack)
    }
})
