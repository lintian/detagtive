// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

const moment = require('moment');
const querystring = require('querystring');

moment.relativeTimeRounding((t) => {
    const DIGITS = 1; // like: 2.5 minutes
    return Math.round(t * Math.pow(10, DIGITS)) / Math.pow(10, DIGITS);
});
moment.relativeTimeThreshold('y', 365);
moment.relativeTimeThreshold('M', 12);
moment.relativeTimeThreshold('w', 4);
moment.relativeTimeThreshold('d', 31);
moment.relativeTimeThreshold('h', 24);
moment.relativeTimeThreshold('m', 60);
moment.relativeTimeThreshold('s', 60);
moment.relativeTimeThreshold('ss', 0);

module.exports = router;

const app = require('../server')
const database = app.database

router.get('/', async (req, res, next) => {

    const get_messages = `
SELECT
    r1.source_name,
    r1.source_version,
    EXTRACT(EPOCH FROM r1.start_time) AS start_epoch,
    EXTRACT(EPOCH FROM r1.end_time) AS end_epoch,
    r1.lintian_version,
    r1.release,
    r1.port,
    r1.messages
FROM
    lintian.runs AS r1
WHERE
    r1.messages <> '{}'
    AND (r1.lintian_version, r1.start_time, r1.release, r1.port) = (
        SELECT
            r2.lintian_version,
            r2.start_time,
            r2.release,
            r2.port
        FROM
            lintian.runs AS r2
        WHERE
            r2.source_name = r1.source_name
            AND r2.source_version = r1.source_version
            AND r2.release = r1.release
            AND r2.port = r1.port
        ORDER BY
            lintian_version DESC,
            start_time DESC,
            release,
            port
        LIMIT 1)
ORDER BY
    source_name,
    source_version DESC
`

    try {
        const dbres_messages = await database.query(get_messages)

        const url_base = '/sources/'
        for (const row of dbres_messages.rows) {

            row.label = row.source_name + ' ' + row.source_version
            row.url = url_base + row.source_name + '?' + querystring.stringify({
                version: row.source_version,
            })

            let elapsed = row.start_epoch - row.end_epoch
            row.duration = moment.duration(elapsed, 'seconds').humanize()
            row.start_locale_string = new Date(row.start_epoch * 1000).toLocaleString()
        }

        res.render('messages', {
            runs: dbres_messages.rows,
        })

    } catch (err) {
        res.send(err.stack)
    }
})
