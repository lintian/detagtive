// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

const path = require('path');
const querystring = require('querystring');

module.exports = router;

const app = require('../server')
const database = app.database

const get_latest_lintian = `
SELECT
    MAX(lintian_version) AS max
FROM
    lintian.tags
`

router.get('/', async (req, res, next) => {

    const get_people = `
SELECT DISTINCT
    phrase AS name
FROM
    archive.mailboxes
ORDER BY
    name ASC
`

    try {
        const dbres = await database.query(get_people)

        for (const row of dbres.rows) {
            row.label = row.name
            row.url = '/people/' + encodeURIComponent(row.name)
        }

        res.render('people', {
            people: dbres.rows
        })

    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/:name', async (req, res) => {

    const get_emails = `
SELECT DISTINCT
    address,
    host
FROM
    archive.mailboxes
WHERE
    phrase = $1
ORDER BY
    host ASC
`

    const get_hints_for_distributor = `
SELECT DISTINCT
    h.source_name,
    h.source_version,
    h.pool_path,
    h.tag_name,
    h.lintian_version,
    h.release,
    h.port,
    t.visibility,
    t.experimental,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    h.override
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
    JOIN archive.distributors AS d ON d.source_name = h.source_name
        AND d.source_version = h.source_version
    JOIN archive.mailboxes AS m ON m.rfc5322 = d.mailbox
WHERE
    m.phrase = $1
    AND t.visibility IN ('error', 'warning') 
    AND t.experimental = FALSE
    AND h.override = FALSE
    AND (h.source_version, h.lintian_version, h.start_time, h.release, h.port) = (
        SELECT
            r.source_version, r.lintian_version, r.start_time, r.release, r.port
        FROM
            lintian.runs AS r
        WHERE
            r.source_name = h.source_name
        ORDER BY
            source_version DESC,
            lintian_version DESC,
            start_time DESC,
            release,
            port
        LIMIT 1)
ORDER BY
    h.source_name,
    h.pool_path,
    t.visibility,
    h.tag_name,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position
`

    const name = req.params.name

    try {

        const dbres_emails = await database.query(get_emails, [name])

        if (!dbres_emails.rows.length)
            throw new Error("Unknown distributor " + name)

        const dbres_latest_lintian = await database.query(get_latest_lintian)
        const latest_lintian = dbres_latest_lintian.rows[0].max

        const dbres_hints = await database.query(get_hints_for_distributor, [name])

        for (const row of dbres_hints.rows) {

            let segments = row.pool_path.split('/')
            row.basename = segments[3]
            row.liberty = segments[0]

            row.label = row.tag_name
            const tag_url_base = '/tags/' + row.tag_name + '?'
            row.url = tag_url_base + querystring.stringify({
                version: row.lintian_version
            })

            row.pointer = ''
            if (row.item_name.length)
                row.pointer += row.item_name
            if (row.item_line_position > 0)
                row.pointer += ':' + row.item_line_position

            if (row.lintian_version == latest_lintian)
                row.current = 1

            row.pointer_url =''
            switch (row.item_index) {
            case 'orig':
                break;
            case 'patched':
                row.pointer_url = 'https://sources.debian.org/src/' +
                    row.source_name + '/' + row.source_version + '/' + row.item_name
                if (row.item_line_position > 0)
                    row.pointer_url += '/#L' + row.item_line_position
                break;
            case 'installed':
                break;
            case 'control':
                let installable_parts = row.basename.split('_')
                row.installable_name = installable_parts[0]
                row.pointer_url = 'https://binarycontrol.debian.net/cache/unstable/' +
                    row.installable_name + '/' + row.item_name
                break;
            }
        }

        const unsorted = {}
        for (const row of dbres_hints.rows) {

            if (!unsorted[row.source_name]) {
                unsorted[row.source_name] = {}
                unsorted[row.source_name].name = row.source_name
                unsorted[row.source_name].version = row.source_version
                const source_url_base = '/sources/' + row.source_name + '?'
                unsorted[row.source_name].url = source_url_base + querystring.stringify({
                    version: row.source_version
                })
                unsorted[row.source_name].lintian_version = row.lintian_version
                unsorted[row.source_name].release = row.release
                unsorted[row.source_name].port = row.port
                unsorted[row.source_name].inputs = {}
            }

            const source = unsorted[row.source_name]

            if (!source.inputs[row.pool_path]) {
                source.inputs[row.pool_path] = {}
                source.inputs[row.pool_path].basename = row.basename
                source.inputs[row.pool_path].liberty = row.liberty
                source.inputs[row.pool_path].hints = []
            }

            source.inputs[row.pool_path].hints.push(row)
        }

        const sources = Object.values(unsorted)

        for (const s of sources) {
            s.inputs = Object.values(s.inputs)

            s.inputs.sort((a, b) => {
                const full_a = a.basename.normalize()
                const full_b = b.basename.normalize()
                const ext_a = path.extname(full_a)
                const ext_b = path.extname(full_b)

                if (ext_a === ext_b)
                    return full_a.localeCompare(full_b)

                // reverse on extension only
                return ext_b.localeCompare(ext_a)
            })
        }

        res.render('single-distributor', {
            name: name,
            emails: dbres_emails.rows,
            sources: sources,
            hint_count: dbres_hints.rows.length,

        })

    } catch (err) {
        res.send(err.stack)
    }
})
