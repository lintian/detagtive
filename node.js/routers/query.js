// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

module.exports = router;

const app = require('../server')
const database = app.database

router.get('/', async (req, res) => {

    try {
        res.render('query-instructions')

    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/runs', async (req, res) => {

    const source = req.query.source

    const get_runs_query = `
SELECT
    r.source_name,
    r.source_version,
    r.start_time,
    r.end_time,
    r.lintian_version,
    r.release,
    r.port,
    r.messages
FROM
    lintian.runs AS r
WHERE
    r.source_name = $1
ORDER BY
    r.source_version DESC,
    r.lintian_version DESC,
    r.start_time DESC,
    r.release,
    r.port
`

    try {
        const dbres = await database.query(get_runs_query, [source])
        res.send(dbres.rows)
    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/releases', async (req, res) => {

    const source = req.query.source

    const get_releases_query = `
SELECT
    rs.source_name,
    rs.source_version,
    rs.release
FROM
    archive.released_sources AS rs
    JOIN archive.sources AS s ON rs.source_name = s.source_name
        AND rs.source_version = s.source_version
WHERE
    s.source_name = $1
    AND EXISTS (
        SELECT
            *
        FROM
            lintian.runs AS r
        WHERE
            r.source_name = rs.source_name
            AND r.source_version = rs.source_version
            AND r.release = rs.release)
`

    try {
        const dbres = await database.query(get_releases_query, [source])
        res.send(dbres.rows)
    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/hints', async (req, res) => {

    const source = req.query.source
    const version = req.query.version

    const get_hints_query = `
SELECT
    h.source_name,
    h.source_version,
    h.pool_path,
    h.start_time,
    h.lintian_version,
    h.release,
    h.port,
    h.tag_name,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    t.visibility,
    m.screen,
    m.excuse,
    h.override,
    h.override_justification
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
    LEFT JOIN lintian.masks AS m ON m.source_name = h.source_name
        AND m.source_version = h.source_version
        AND m.lintian_version = h.lintian_version
        AND m.start_time = h.start_time
        AND m.release = h.release
        AND m.port = h.port
        AND m.pool_path = h.pool_path
        AND m.position = h.position
WHERE
    h.source_name = $1
    AND h.source_version = $2
    AND (h.lintian_version, h.start_time, h.release, h.port) = (
        SELECT
            r.lintian_version, r.start_time, r.release, r.port
        FROM
            lintian.runs AS r
        WHERE
            r.source_name = h.source_name
            AND r.source_version = h.source_version
        ORDER BY
            r.lintian_version DESC,
            r.start_time DESC,
            r.release,
            r.port
        LIMIT 1)
`

    try {
        const dbres = await database.query(get_hints_query, [source, version])
        res.send(dbres.rows)
    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/contexts', async (req, res) => {

    const tag = req.query.tag

    const get_contexts_query = `
SELECT
    h.source_name,
    h.source_version,
    h.pool_path,
    h.start_time,
    h.lintian_version,
    h.release,
    h.port,
    h.tag_name,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    t.visibility,
    m.screen,
    m.excuse,
    h.override,
    h.override_justification
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
    LEFT JOIN lintian.masks AS m ON m.source_name = h.source_name
        AND m.source_version = h.source_version
        AND m.lintian_version = h.lintian_version
        AND m.start_time = h.start_time
        AND m.release = h.release
        AND m.port = h.port
        AND m.pool_path = h.pool_path
        AND m.position = h.position
WHERE
    h.tag_name = $1
    AND (h.lintian_version, h.start_time, h.release, h.port) = (
        SELECT
            r.lintian_version, r.start_time, r.release, r.port
        FROM
            lintian.runs AS r
        WHERE
            r.source_name = h.source_name
            AND r.source_version = h.source_version
        ORDER BY
            r.lintian_version DESC,
            r.start_time DESC,
            release,
            port
        LIMIT 1)
`

    try {
        const dbres = await database.query(get_contexts_query, [tag])
        res.send(dbres.rows)
    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/quality-count/:name', async (req, res) => {

    const source_name = req.params.name

    const get_counts_query = `
WITH relevant_run AS (
    SELECT
        r2.source_name,
        r2.source_version,
        r2.lintian_version,
        r2.start_time,
        r2.release,
        r2.port
    FROM
        lintian.runs AS r2
    WHERE
        r2.source_name = $1
    ORDER BY
        r2.source_name DESC,
        r2.source_version DESC,
        r2.lintian_version DESC,
        r2.start_time DESC,
        r2.release,
        r2.port
    LIMIT 1
)
SELECT
    r1.source_name,
    r1.source_version,
    r1.start_time,
    r1.lintian_version,
    r1.release,
    r1.port,
    COUNT(
        CASE WHEN (t.visibility = 'error'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS errors,
    COUNT(
        CASE WHEN (t.visibility = 'warning'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS warnings,
    COUNT(
        CASE WHEN (t.visibility = 'info'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS info,
    COUNT(
        CASE WHEN (t.visibility = 'pedantic'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS pedantic,
    COUNT(
        CASE WHEN (t.experimental = TRUE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS experimental,
    COUNT(
        CASE WHEN (h.override = TRUE) THEN
            1
        ELSE
            NULL
        END) AS overrides
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
    JOIN relevant_run AS r1 ON r1.source_name = h.source_name
        AND r1.source_version = h.source_version
        AND r1.lintian_version = h.lintian_version
        AND r1.start_time = h.start_time
        AND r1.release = h.release
        AND r1.port = h.port
GROUP BY
    r1.source_name,
    r1.source_version,
    r1.lintian_version,
    r1.start_time,
    r1.release,
    r1.port
`

    try {
        const dbres = await database.query(get_counts_query, [source_name])
        res.send(dbres.rows)
    } catch (err) {
        res.send(err.stack)
    }
})
