// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

const path = require('path');
const querystring = require('querystring');

module.exports = router;

const app = require('../server')
const database = app.database

const get_latest_lintian = `
SELECT
    MAX(lintian_version) AS max
FROM
    lintian.tags
`

router.get('/', async (req, res, next) => {

    const get_screens = `
SELECT
    s1.screen,
    s1.lintian_version,
    s1.tag_name,
    t.visibility
FROM
    lintian.screens AS s1
    JOIN lintian.tags AS t ON t.tag_name = s1.tag_name
        AND t.lintian_version = s1.lintian_version
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            lintian.screens AS s2
        WHERE
            s2.screen = s1.screen
            AND s2.lintian_version > s1.lintian_version)
ORDER BY
    s1.screen
`

    try {
        const dbres_latest_lintian = await database.query(get_latest_lintian)
        const latest_lintian = dbres_latest_lintian.rows[0].max

        const dbres_screens = await database.query(get_screens)
        let screens = dbres_screens.rows;

        for (const screen of screens) {
            screen.screen_url = '/screens/' + screen.screen
            screen.tag_url = '/tags/' + screen.tag_name
            if (screen.lintian_version != latest_lintian)
                screen.old = 1
        }

        res.render('screens', {
            screens: screens
        })

    } catch (err) {
        res.send(err.stack)
    }
})

router.get(/^\/(.+)$/, async (req, res) => {

    const get_screens = `
SELECT
    s.screen,
    s.tag_name,
    s.lintian_version,
    v.repository_reference,
    s.reason_html,
    s.see_also_html,
    t.visibility
FROM
    lintian.screens AS s
    LEFT JOIN lintian.tags AS t ON t.tag_name = s.tag_name
       AND t.lintian_version = s.lintian_version
    JOIN lintian.versions AS v ON v.lintian_version = t.lintian_version
WHERE s.screen = $1
GROUP BY
    s.screen,
    s.tag_name,
    s.lintian_version,
    v.repository_reference,
    t.visibility
ORDER BY s.lintian_version DESC
`

    const get_hints_for_screen = `
SELECT DISTINCT
    m.source_name,
    m.source_version,
    m.pool_path,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    m.excuse,
    h.override,
    h.override_justification
FROM
    lintian.masks AS m
JOIN lintian.hints AS h ON h.source_name = m.source_name
    AND h.source_version = m.source_version
    AND h.lintian_version = m.lintian_version
    AND h.start_time = m.start_time
    AND h.release = m.release
    AND h.port = m.port
    AND h.pool_path = m.pool_path
    AND h.position = m.position
WHERE
    m.screen = $1
    AND (h.source_version, h.lintian_version, h.start_time, h.release, h.port) = (
        SELECT
            r.source_version, r.lintian_version, r.start_time, r.release, r.port
        FROM
            lintian.runs AS r
        WHERE
            r.source_name = h.source_name
        ORDER BY
            source_version DESC,
            lintian_version DESC,
            start_time DESC,
            release,
            port
        LIMIT 1)
ORDER BY
    m.source_name,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    m.excuse,
    h.override,
    h.override_justification
`

    const name = req.params[0]

    let requested_version = req.query.version

    try {
        const dbres_screens = await database.query(get_screens, [name])

        if (!dbres_screens.rows.length) {
            throw new Error("Unknown screen " + name)
        }

        let screens = dbres_screens.rows
        const screen_url_base = '/screens/' + name + '?'

        for (const screen of screens) {
            screen.label = screen.lintian_version
            screen.url = screen_url_base + querystring.stringify({
                version: screen.lintian_version
            })
            screen.highlight = false
        }

        if (screens.length && !requested_version)
            requested_version = screens[0].lintian_version

        let selected = screens[0];

        const dbres_latest_lintian = await database.query(get_latest_lintian)
        const latest_lintian = dbres_latest_lintian.rows[0].max

        let deleted = true

        for (const screen of screens) {
            screen.tag_url = '/tags/' + screen.tag_name
            if (screen.lintian_version == requested_version)
                selected = screen
            if (screen.lintian_version == latest_lintian)
                deleted = false
        }

        selected.highlight = true
        if (selected.see_also_html.length) {
            selected.see_also_statement = 'Please refer to ' + oxford_enumeration('and', selected.see_also_html) + ' for details.'
        }

        if (selected.see_also_html.length) {
            selected.see_also_statement = 'Read more in ' + oxford_enumeration('and', selected.see_also_html) + '.'
        }

        selected.release_tag = selected.lintian_version.replace(/[.]\d+$/, '.0')
        selected.short_commit = selected.repository_reference.substring(0, 7)

        let relative = selected.screen
            .replace(/^(.)/g, (match, p1) => p1.toUpperCase())
            .replace(/[/](.)/g, (match, p1) => '/' + p1.toUpperCase())
            .replace(/-(.)/g, (match, p1) => p1.toUpperCase())

        const repo_path = 'lib/Lintian/Screen/' + relative + '.pm'

        selected.release_tag_url = 'https://salsa.debian.org/lintian/lintian/-/tags/' + selected.release_tag
        selected.commit_url = 'https://salsa.debian.org/lintian/lintian/-/blob/' + selected.repository_reference
            + '/' + repo_path
        selected.screen_url = 'https://salsa.debian.org/lintian/lintian/-/tree/master/'
            + repo_path

        const unsorted = {}
        let hint_count = 0
        let override_count = 0
        let performance_percentage;

        if (selected.visibility != 'classification') {
            const dbres_hints = await database.query(get_hints_for_screen, [name])

            hint_count = dbres_hints.rows.length

            for (const row of dbres_hints.rows) {

                let segments = row.pool_path.split('/')
                row.basename = segments[3]
                row.liberty = segments[0]

                row.visibility = 'mask'

                if (row.override) {
                    row.visibility = 'override'
                    override_count++
                }

                row.pointer = ''
                if (row.item_name.length)
                    row.pointer += row.item_name
                if (row.item_line_position > 0)
                    row.pointer += ':' + row.item_line_position

                row.pointer_url =''
                switch (row.item_index) {
                case 'orig':
                    break;
                case 'patched':
                    row.pointer_url = 'https://sources.debian.org/src/' +
                        row.source_name + '/' + row.source_version + '/' + row.item_name
                    if (row.item_line_position > 0)
                        row.pointer_url += '/#L' + row.item_line_position
                    break;
                case 'installed':
                    break;
                case 'control':
                    let installable_parts = row.basename.split('_')
                    row.installable_name = installable_parts[0]
                    row.pointer_url = 'https://binarycontrol.debian.net/cache/unstable/' +
                        row.installable_name + '/' + row.item_name
                    break;
                }
            }

            for (const row of dbres_hints.rows) {

                if (!unsorted[row.source_name]) {
                    unsorted[row.source_name] = {}
                    unsorted[row.source_name].name = row.source_name
                    unsorted[row.source_name].version = row.source_version
                    const source_url_base = '/sources/' + row.source_name + '?'
                    unsorted[row.source_name].url = source_url_base + querystring.stringify({
                        version: row.source_version
                    })
                    unsorted[row.source_name].lintian_version = row.lintian_version
                    unsorted[row.source_name].release = row.release
                    unsorted[row.source_name].port = row.port
                    unsorted[row.source_name].inputs = {}
                }

                const source = unsorted[row.source_name]

                if (!source.inputs[row.pool_path]) {
                    source.inputs[row.pool_path] = {}
                    source.inputs[row.pool_path].basename = row.basename
                    source.inputs[row.pool_path].liberty = row.liberty
                    source.inputs[row.pool_path].hints = []
                }

                source.inputs[row.pool_path].hints.push(row)
            }
        }

        const sources = Object.values(unsorted)

        for (const s of sources) {
            s.inputs = Object.values(s.inputs)

            s.inputs.sort((a, b) => {
                const full_a = a.basename.normalize()
                const full_b = b.basename.normalize()
                const ext_a = path.extname(full_a)
                const ext_b = path.extname(full_b)

                if (ext_a === ext_b)
                    return full_a.localeCompare(full_b)

                // reverse on extension only
                return ext_b.localeCompare(ext_a)
            })
        }

        res.render('single-screen', {
            name: name,
            screens: screens,
            selected: selected,
            sources: sources,
            hint_count: hint_count,
            override_count: override_count,
            latest_lintian: latest_lintian,
            deleted: deleted
        })

    } catch (err) {
        res.send(err.stack)
    }
})

function oxford_enumeration(conjunctive, alternatives) {

    if (!alternatives.length)
        return ''

    // remove and save last element
    const last = alternatives.pop()

    let text = ''
    const maybe_comma = (alternatives.length > 1 ? ',' : '')

    if (alternatives.length)
        text = alternatives.join(', ') + maybe_comma + ' ' + conjunctive + ' '

    text += last

    return text;
}
