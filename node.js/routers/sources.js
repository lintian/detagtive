// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

const moment = require('moment');
const path = require('path');
const querystring = require('querystring');

moment.relativeTimeRounding((t) => {
    const DIGITS = 1; // like: 2.5 minutes
    return Math.round(t * Math.pow(10, DIGITS)) / Math.pow(10, DIGITS);
});
moment.relativeTimeThreshold('y', 365);
moment.relativeTimeThreshold('M', 12);
moment.relativeTimeThreshold('w', 4);
moment.relativeTimeThreshold('d', 31);
moment.relativeTimeThreshold('h', 24);
moment.relativeTimeThreshold('m', 60);
moment.relativeTimeThreshold('s', 60);
moment.relativeTimeThreshold('ss', 0);

module.exports = router;

const app = require('../server')
const database = app.database

router.get('/', async (req, res, next) => {

    const get_sources = `
SELECT DISTINCT
    source_name
FROM
    archive.sources
ORDER BY source_name ASC
`

    try {
        const dbres = await database.query(get_sources)
        let sources = [];
        for (const row of dbres.rows)
            sources.push({
                name: row.source_name,
                url: '/sources/' + row.source_name
            })
        res.render('sources', {
            sources: sources
        })

    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/:name', async (req, res) => {

    const get_source_versions = `
SELECT
    s.source_version,
    rs.release,
    MAX(s.source_liberty) AS source_liberty,
    MAX(s.homepage) AS homepage,
    MAX(s.vcs_browser) AS vcs_browser
FROM
    archive.sources AS s
    LEFT JOIN archive.released_sources AS rs ON rs.source_name = s.source_name
        AND rs.source_version = s.source_version
WHERE
    s.source_name = $1
    AND release NOT LIKE '%-debug'
GROUP BY
    s.source_version,
    rs.release
ORDER BY
    source_version DESC,
    release
`
    const get_runs = `
SELECT
    EXTRACT(EPOCH FROM r.start_time) AS start_epoch,
    EXTRACT(EPOCH FROM r.end_time) AS end_epoch,
    r.start_time,
    r.lintian_version,
    v.repository_reference,
    r.invocation_options,
    r.exit_status,
    r.release,
    r.port,
    r.messages
FROM
    lintian.runs AS r
    JOIN lintian.versions AS v ON v.lintian_version = r.lintian_version
WHERE
    r.source_name = $1
    AND r.source_version = $2
ORDER BY
    r.lintian_version DESC,
    r.start_time DESC,
    r.release,
    r.port
`
    const get_hints = `
SELECT
    h.source_name,
    h.source_version,
    h.release,
    h.pool_path,
    h.tag_name,
    t.visibility,
    t.experimental,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    m.screen,
    m.excuse,
    h.override,
    h.override_justification
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
    LEFT JOIN lintian.masks AS m ON m.source_name = h.source_name
        AND m.source_version = h.source_version
        AND m.lintian_version = h.lintian_version
        AND m.start_time = h.start_time
        AND m.release = h.release
        AND m.port = h.port
        AND m.pool_path = h.pool_path
        AND m.position = h.position
WHERE
    h.source_name = $1
    AND h.source_version = $2
    AND h.lintian_version = $3
    AND h.start_time = $4
    AND h.release = $5
    AND h.port = $6
    AND t.visibility <> 'classification'
ORDER BY
    h.pool_path,
    m.screen DESC,
    h.override,
    t.experimental,
    t.visibility,
    h.tag_name,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position
`

    const source_name = req.params.name

    let source_version = req.query.version
    let age = parseInt(req.query.age)

    const regex = /[.]html$/
    if (regex.test(source_name)) {
        const new_url = source_name.replace(/[.]html$/, "")
        res.redirect(301, '/sources/' + new_url)
        return
    }

    try {
        const dbres_sources = await database.query(get_source_versions, [source_name])

        if (!dbres_sources.rows.length)
            throw new Error("Unknown source " + source_name)

        let releases = {}
        let found = 0

        for (const row of dbres_sources.rows) {
            if (!releases[row.source_version]) {
                releases[row.source_version] = {}
                releases[row.source_version].version = row.source_version
                releases[row.source_version].source_liberty = row.source_liberty
                releases[row.source_version].homepage = row.homepage
                releases[row.source_version].vcs_browser = row.vcs_browser
                releases[row.source_version].releases = []
            }

            if (source_version == row.source_version)
                found = 1

            if (row.release.length)
                releases[row.source_version].releases.push(row.release)
        }

        if (source_version && !found) {
            res.redirect(302, '/sources/' + source_name)
            return
        }

        const sources = Object.values(releases)

        const url_base = '/sources/' + source_name + '?'

        for (let s of sources) {
            s.label = s.version
            s.url = url_base + querystring.stringify({
                version: s.version
            })
            s.highlight = false
        }

        if (sources.length && !source_version)
            source_version = sources[0].version

        let maintenance_links = []
        maintenance_links.push({
            label: "Tracker",
            url: "https://tracker.debian.org/pkg/" + source_name
        })

        let source_liberty

        for (let s of sources) {
            if (s.version == source_version) {
                s.highlight = true

                source_liberty = s.source_liberty

                if (s.homepage.length)
                    maintenance_links.push({
                        label: "Homepage",
                        url: s.homepage
                    })

                if (s.vcs_browser.length)
                    maintenance_links.push({
                        label: "Vcs-Browser",
                        url: s.vcs_browser
                    })
            }
        }

        const dbres_runs = await database.query(get_runs,
            [source_name, source_version])

        let runs = dbres_runs.rows

        let position = 0
        for (let run of runs) {
            run.label = moment.duration(run.start_epoch - Date.now() / 1000, 'seconds').humanize(true) + " (" + run.release + ", " + run.port + ")"
            run.url = url_base + querystring.stringify({
                version: source_version,
                age: position
            })
            run.highlight = false

            position++
        }

        let selected

        if (runs.length) {

            if (isNaN(age) || age < 0 || age > runs.length - 1)
                age = 0

            selected = runs[age]

            selected.highlight = true
            selected.start_locale_string = new Date(selected.start_epoch * 1000).toLocaleString();

            let elapsed = selected.start_epoch - selected.end_epoch
            selected.duration = moment.duration(elapsed, 'seconds').humanize();

            selected.release_tag = selected.lintian_version.replace(/[.]\d+$/, '.0')
            selected.release_tag_url = 'https://salsa.debian.org/lintian/lintian/-/tags/' + selected.release_tag
            selected.commit_url = 'https://salsa.debian.org/lintian/lintian/-/tree/' + selected.repository_reference
            selected.short_commit = selected.repository_reference.substring(0, 7)

            const dbres_hints = await database.query(get_hints,
                [source_name, source_version, selected.lintian_version, selected.start_time, selected.release, selected.port])

            const tag_url_base = '/tags/'

            for (const h of dbres_hints.rows) {

                h.label = h.tag_name

                if (h.experimental) {
                    h.visibility = 'experimental'
                }

                if (h.override) {
                    h.visibility = 'override'
                }

                if (h.screen) {
                    h.visibility = 'mask'
                    h.screen_url = '/screens/' + h.screen
                }

                let segments = h.pool_path.split('/')
                h.basename = segments[3]
                h.liberty = segments[0]

                h.url = tag_url_base + h.tag_name + '?' + querystring.stringify({
                    version: selected.lintian_version
                })

                h.pointer = ''
                if (h.item_name.length)
                    h.pointer += h.item_name
                if (h.item_line_position > 0)
                    h.pointer += ':' + h.item_line_position

                h.pointer_url = ''
                switch (h.item_index) {
                case 'orig':
                    break;
                case 'patched':
                    h.pointer_url = 'https://sources.debian.org/src/' +
                        h.source_name + '/' + h.source_version + '/' + h.item_name
                    if (h.item_line_position > 0)
                        h.pointer_url += '/#L' + h.item_line_position
                    break;
                case 'installed':
                    break;
                case 'control':
                    let installable_parts = h.basename.split('_')
                    h.installable_name = installable_parts[0]
                    h.pointer_url = 'https://binarycontrol.debian.net/cache/unstable/' +
                        h.installable_name + '/' + h.item_name
                    break;
                }
            }

            let inputs = {}
            for (const h of dbres_hints.rows) {
                if (!inputs[h.pool_path]) {
                    inputs[h.pool_path] = {}
                    inputs[h.pool_path].basename = h.basename
                    inputs[h.pool_path].liberty = h.liberty
                    inputs[h.pool_path].hints = []
                }

                inputs[h.pool_path].hints.push(h)
            }

            selected.inputs = Object.values(inputs)

            selected.inputs.sort((a, b) => {
                const full_a = a.basename.normalize()
                const full_b = b.basename.normalize()
                const ext_a = path.extname(full_a)
                const ext_b = path.extname(full_b)

                if (ext_a === ext_b)
                    return full_a.localeCompare(full_b)

                // reverse on extension only
                return ext_b.localeCompare(ext_a)
            })
        }

        res.render('single-source', {
            source_name: source_name,
            source_version: source_version,
            source_liberty: source_liberty,
            maintenance_links: maintenance_links,
            sources: sources,
            runs: runs,
            selected: selected
        })

    } catch (err) {
        res.send(err.stack)
    }
})

router.get('/:name/:version', async (req, res) => {

    const source_name = req.params.name
    const source_version = req.params.version

    const regex = /[.]html$/
    const new_version = source_version.replace(/[.]html$/, "")

    res.redirect(301, '/sources/' + source_name + '?' + querystring.stringify({
        version: new_version
    }))
})
