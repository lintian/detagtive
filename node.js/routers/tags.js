// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express');
const router = express.Router();

const querystring = require('querystring');

module.exports = router;

const app = require('../server')
const database = app.database

const get_latest_lintian = `
SELECT
    MAX(lintian_version) AS max
FROM
    lintian.tags
`

router.get('/', async (req, res, next) => {

    // adapted from https://stackoverflow.com/a/9472858
    const get_tags = `
WITH potential_tags AS (
    SELECT
        t.tag_name AS apparent_name,
        t.tag_name,
        t.lintian_version,
        FALSE AS renamed
    FROM
        lintian.tags AS t
    UNION
    SELECT
        rt.renamed_from AS apparent_name,
        rt.tag_name,
        rt.lintian_version,
        TRUE AS renamed
    FROM
        lintian.renamed_tags AS rt
)
SELECT
    p1.apparent_name,
    p1.tag_name,
    p1.lintian_version,
    t.visibility,
    p1.renamed
FROM
    potential_tags AS p1
    JOIN lintian.tags AS t ON t.tag_name = p1.tag_name
        AND t.lintian_version = p1.lintian_version
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            potential_tags AS p2
        WHERE
            p2.apparent_name = p1.apparent_name
            AND p2.lintian_version > p1.lintian_version)
ORDER BY
    apparent_name
`

    try {
        const dbres_latest_lintian = await database.query(get_latest_lintian)
        const latest_lintian = dbres_latest_lintian.rows[0].max

        const dbres_tags = await database.query(get_tags)
        let tags = dbres_tags.rows;

        for (const t of tags) {
            t.label = t.apparent_name
            t.url = '/tags/' + t.tag_name
            if (t.lintian_version != latest_lintian)
                t.old = 1
        }

        res.render('tags', {
            tags: tags
        })

    } catch (err) {
        res.send(err.stack)
    }
})

router.get(/^\/(.+)$/, async (req, res) => {

    const get_tags = `
SELECT
    t.tag_name,
    t.lintian_version,
    v.repository_reference,
    array_remove(array_agg(rt.renamed_from), NULL) AS renamed_from,
    t.visibility,
    t.experimental,
    t.name_spaced,
    t.show_always,
    t."check",
    t.explanation_html,
    t.see_also_html
FROM
    lintian.tags AS t
    JOIN lintian.versions AS v ON v.lintian_version = t.lintian_version
    LEFT JOIN lintian.renamed_tags AS rt ON rt.tag_name = t.tag_name
       AND rt.lintian_version = t.lintian_version
WHERE t.tag_name = $1
GROUP BY
    t.tag_name,
    t.lintian_version,
    v.repository_reference
ORDER BY t.lintian_version DESC
`

    const get_replacement = `
SELECT
    rt.tag_name,
    rt.lintian_version
FROM
    lintian.renamed_tags AS rt
WHERE
    rt.renamed_from = $1
ORDER BY
    rt.lintian_version DESC,
    rt.tag_name
LIMIT 1
`

        const get_screens = `
SELECT
    s.screen
FROM
    lintian.screens AS s
WHERE
    s.tag_name = $1
    AND s.lintian_version = $2
ORDER BY
    s.screen
`

    const get_hints_for_tag = `
SELECT DISTINCT
    h.source_name,
    h.source_version,
    h.pool_path,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    m.screen,
    m.excuse,
    h.override,
    h.override_justification
FROM
    lintian.hints AS h
LEFT JOIN lintian.masks AS m ON m.source_name = h.source_name
    AND m.source_version = h.source_version
    AND m.lintian_version = h.lintian_version
    AND m.start_time = h.start_time
    AND m.release = h.release
    AND m.port = h.port
    AND m.pool_path = h.pool_path
    AND m.position = h.position
WHERE
    h.tag_name = $1
    AND (h.source_version, h.lintian_version, h.start_time, h.release, h.port) = (
        SELECT
            r.source_version, r.lintian_version, r.start_time, r.release, r.port
        FROM
            lintian.runs AS r
        WHERE
            r.source_name = h.source_name
        ORDER BY
            source_version DESC,
            lintian_version DESC,
            start_time DESC,
            release,
            port
        LIMIT 1)
ORDER BY
    h.source_name,
    h.note,
    h.item_index,
    h.item_name,
    h.item_line_position,
    m.screen,
    m.excuse,
    h.override,
    h.override_justification
`

    const name = req.params[0]

    let requested_version = req.query.version

    const regex = /[.]html$/
    if (regex.test(name)) {
        const new_url = name.replace(/[.]html$/, "")
        res.redirect(301, '/tags/' + new_url)
        return
    }

    try {
        const dbres_tags = await database.query(get_tags, [name])

        if (!dbres_tags.rows.length) {

            const dbres_replacement = await database.query(get_replacement, [name])
            if (!dbres_replacement.rows.length)
                throw new Error("Unknown tag " + name)

            let new_name = dbres_replacement.rows[0].tag_name;

            res.redirect(301, '/tags/' + new_name)
            return
        }

        let tags = dbres_tags.rows
        const tag_url_base = '/tags/' + name + '?'

        for (const t of tags) {
            t.label = t.lintian_version
            t.url = tag_url_base + querystring.stringify({
                version: t.lintian_version
            })
            t.highlight = false
        }

        if (tags.length && !requested_version)
            requested_version = tags[0].lintian_version

        let selected = tags[0];

        const dbres_latest_lintian = await database.query(get_latest_lintian)
        const latest_lintian = dbres_latest_lintian.rows[0].max

        let deleted = true

        for (const t of tags) {
            if (t.lintian_version == requested_version)
                selected = t
            if (t.lintian_version == latest_lintian)
                deleted = false
        }

        selected.highlight = true

        selected.release_tag = selected.lintian_version.replace(/[.]\d+$/, '.0')
        selected.short_commit = selected.repository_reference.substring(0, 7)

        let relative = selected.check
            .replace(/^(.)/g, (match, p1) => p1.toUpperCase())
            .replace(/[/](.)/g, (match, p1) => '/' + p1.toUpperCase())
            .replace(/-(.)/g, (match, p1) => p1.toUpperCase())

        const repo_path = 'lib/Lintian/Check/' + relative + '.pm'

        selected.release_tag_url = 'https://salsa.debian.org/lintian/lintian/-/tags/' + selected.release_tag
        selected.commit_url = 'https://salsa.debian.org/lintian/lintian/-/blob/' + selected.repository_reference
            + '/' + repo_path
        selected.check_url = 'https://salsa.debian.org/lintian/lintian/-/tree/master/'
            + repo_path

        let sorted = []
        let hint_count = 0
        let mask_count = 0
        let override_count = 0
        let performance_percentage;

        if (selected.visibility != 'classification') {
            const dbres_hints = await database.query(get_hints_for_tag, [name])

            hint_count = dbres_hints.rows.length

            for (const row of dbres_hints.rows) {

                let segments = row.pool_path.split('/')
                row.basename = segments[3]
                row.liberty = segments[0]

                row.visibility = selected.visibility

                if (row.override) {
                    row.visibility = 'override'
                    override_count++
                }

                if (row.screen) {
                    row.visibility = 'mask'
                    row.screen_url = '/screens/' + row.screen
                    mask_count++
                }

                row.pointer = ''
                if (row.item_name.length)
                    row.pointer += row.item_name
                if (row.item_line_position > 0)
                    row.pointer += ':' + row.item_line_position

                row.pointer_url =''
                switch (row.item_index) {
                case 'orig':
                    break;
                case 'patched':
                    row.pointer_url = 'https://sources.debian.org/src/' +
                        row.source_name + '/' + row.source_version + '/' + row.item_name
                    if (row.item_line_position > 0)
                        row.pointer_url += '/#L' + row.item_line_position
                    break;
                case 'installed':
                    break;
                case 'control':
                    let installable_parts = row.basename.split('_')
                    row.installable_name = installable_parts[0]
                    row.pointer_url = 'https://binarycontrol.debian.net/cache/unstable/' +
                        row.installable_name + '/' + row.item_name
                    break;
                }
            }

            let sources = {}
            for (const row of dbres_hints.rows) {

                if (!sources[row.source_name]) {
                    sources[row.source_name] = {}
                    sources[row.source_name].name = row.source_name
                    sources[row.source_name].hints = []
                }

                sources[row.source_name].hints.push(row)
            }

            performance_percentage = Math.round(100 * (1 - override_count / hint_count))

            for (const source_name in sources)
                sorted.push(sources[source_name])

            const source_url_base = '/sources/'

            for (const s of sorted) {
                s.url = source_url_base + s.name
            }
        }

        const dbres_screens = await database.query(get_screens, [selected.tag_name, selected.lintian_version])
        selected.screens = dbres_screens.rows

        for (const screen of selected.screens) {

            screen.url = '/screens/' + screen.screen
        }

        res.render('single-tag', {
            name: name,
            tags: tags,
            selected: selected,
            sources: sorted,
            hint_count: hint_count,
            mask_count: mask_count,
            override_count: override_count,
            performance_percentage: performance_percentage,
            latest_lintian: latest_lintian,
            deleted: deleted
        })

    } catch (err) {
        res.send(err.stack)
    }
})

function oxford_enumeration(conjunctive, alternatives) {

    if (!alternatives.length)
        return ''

    // remove and save last element
    const last = alternatives.pop()

    let text = ''
    const maybe_comma = (alternatives.length > 1 ? ',' : '')

    if (alternatives.length)
        text = alternatives.join(', ') + maybe_comma + ' ' + conjunctive + ' '

    text += last

    return text;
}
