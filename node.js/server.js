// Copyright © 2021 Felix Lechner

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const fs = require('fs')
const yaml = require('js-yaml')

var config

try {
    const home = process.env.HOME || '.';

    let fileContents = fs.readFileSync(home + '/website.config', 'utf8')
    config = yaml.safeLoad(fileContents)

} catch (e) {
    console.log(e)
}

const datastore = config.datastore
const web = config.web

const {
    Pool
} = require('pg')

const pool = new Pool(datastore)

pool.on('error', (err, client) => {
    console.error('Error:', err);
});

process.on('exit', function() {
    pool.end()
})

exports.database = pool

const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const app = express()
const port = web.port

morgan.token('url', (req, res) => decodeURI(req.originalUrl || req.url))

app.use(helmet())
app.use(morgan('combined'))

var options = {
    setHeaders: function (res, path, stat) {
       let regex = new RegExp('/schema/json/')
       if (regex.test(path))
           res.type('application/schema+json')
    }
}
app.use(express.static('../public', options))

app.set('views', './views');
app.set('view engine', 'pug');

const autoreject = require('./routers/autoreject')
const people = require('./routers/people')
const tags = require('./routers/tags')
const screens = require('./routers/screens')
const sources = require('./routers/sources')
const manual = require('./routers/manual')
const messages = require('./routers/messages')
const levels = require('./routers/levels')
const query = require('./routers/query')

app.use('/people', people)
app.use('/query', query)
app.use('/screens', screens)
app.use('/sources', sources)
app.use('/manual', manual)
app.use('/levels', levels)
app.use('/messages', messages)
app.use('/tags', tags)
app.use('/autoreject', autoreject)

app.get('/reports/:group/:selector', async (req, res) => {
    res.redirect(301, '/' + req.params.group + '/' + req.params.selector)
})

app.get('/full/:remainder', async (req, res) => {
    res.redirect(301, '/maintainer/' + req.params.remainder)
})

app.get('/maintainer/:address', async (req, res) => {

    const get_names = `
SELECT DISTINCT
    phrase AS name
FROM
    archive.mailboxes
WHERE
    address = $1
ORDER BY
    name ASC
LIMIT 1
`

    let address = req.params.address

    const regex = /[.]html$/
    if (regex.test(address))
        address = address.replace(/[.]html$/, "")

    try {

        const dbres_names = await pool.query(get_names, [address])

        if (!dbres_names.rows.length)
            throw new Error("Unknown distributor address " + address)

        const name = dbres_names.rows[0].name

        res.redirect(301, '/people/' + name)

    } catch (err) {
        res.send(err.stack)
    }
})

app.get('/', async (req, res, next) => {
    res.render('index')
})

app.get('*', async (req, res, next) => {
    res.status(200).send('Sorry, page not found');
    next();
})

app.listen(port, () => console.log(`Listening on port ${port}.`))
