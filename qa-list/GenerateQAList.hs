-- Copyright © 2021 Felix Lechner
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import qualified Data.ByteString.UTF8          as BU
import           Data.Foldable                  ( for_ )
import           Data.Int                       ( Int64(..) )
import           Data.List                      ( unwords )
import           Data.Text                      ( Text(..) )
import qualified Data.Text                     as Text
import           Data.Vector                    ( Vector(..) )
import qualified Data.Yaml                     as Yaml
import           GHC.Generics                   ( Generic )
import qualified Hasql.Connection              as Connection
import           Hasql.Session                  ( Session )
import qualified Hasql.Session                 as Session
import           Hasql.Statement                ( Statement(..) )
import qualified Hasql.TH                      as TH
import qualified Options.Applicative           as Options

data CommandLine = CommandLine
  { configPath :: String
  }

parser :: Options.Parser CommandLine
parser = CommandLine <$> Options.strOption
  (  Options.long "config"
  <> Options.short 'c'
  <> Options.metavar "PATH"
  <> Options.help "Path to configuration file"
  )

newtype Config = Config
  { database :: String
  }
  deriving (Show, Generic)

instance Yaml.FromJSON Config

defaultConfig = Config { database = "" }

getConfig :: String -> IO Config
getConfig configPath = do
  eitherConfig <-
    Yaml.decodeFileEither configPath :: IO (Either Yaml.ParseException Config)

  case eitherConfig of
    Right config -> return config
    Left  error  -> do
      print error
      return defaultConfig

qaCountStatement
  :: Statement () (Vector (Text, Int64, Int64, Int64, Int64, Int64, Int64))
qaCountStatement = [TH.vectorStatement|
SELECT
    h.source_name::text,
    COUNT(
        CASE WHEN (t.visibility = 'error'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END)::int8 AS errors,
    COUNT(
        CASE WHEN (t.visibility = 'warning'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END)::int8 AS warnings,
    COUNT(
        CASE WHEN (t.visibility = 'info'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END)::int8 AS info,
    COUNT(
        CASE WHEN (t.visibility = 'pedantic'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END)::int8 AS pedantic,
    COUNT(
        CASE WHEN (t.experimental = TRUE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END)::int8 AS experimental,
    COUNT(
        CASE WHEN (h.override = TRUE) THEN
            1
        ELSE
            NULL
        END)::int8 AS overrides
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
WHERE (h.source_name, h.source_version, h.lintian_version, h.start_time, h.release, h.port) = (
    SELECT
        r2.source_name,
        r2.source_version,
        r2.lintian_version,
        r2.start_time,
        r2.release,
        r2.port
    FROM
        lintian.runs AS r2
    WHERE
        r2.source_name = h.source_name
    ORDER BY
        r2.source_name DESC,
        r2.source_version DESC,
        r2.lintian_version DESC,
        r2.start_time DESC,
        r2.release,
        r2.port
    LIMIT 1)
GROUP BY
    h.source_name,
    h.source_version,
    h.lintian_version,
    h.start_time,
    h.release,
    h.port
ORDER BY
    h.source_name
    |]

mySession :: Session (Vector (Text, Int64, Int64, Int64, Int64, Int64, Int64))
mySession = do
  Session.statement () qaCountStatement

generateQaList :: CommandLine -> IO ()
generateQaList (CommandLine path) = do
  config           <- getConfig path
  Right connection <- Connection.acquire $ BU.fromString (database config)
  Right result     <- Session.run mySession connection
  for_ result
    $ \(name, errors, warnings, info, pedantic, experimental, overrides) ->
        putStrLn $ unwords
          (Text.unpack name : map
            show
            [errors, warnings, info, pedantic, experimental, overrides]
          )
  Connection.release connection
generateQaList _ = return ()

main :: IO ()
main = generateQaList =<< Options.execParser opts
 where
  opts = Options.info
    (parser Options.<**> Options.helper)
    (  Options.fullDesc
    <> Options.progDesc "Generate QA List from Lintian's database"
    <> Options.header "generate-qa-list - generate qa-list.txt "
    )
