#!/usr/bin/perl

# Copyright © 2019-2021 Felix Lechner
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA

use v5.20;
use warnings;
use utf8;
use autodie;

use Const::Fast;
use DBI;
use Getopt::Long;
use Unicode::UTF8 qw(encode_utf8);
use YAML::XS qw(LoadFile);

const my $EMPTY => q{};
const my $SPACE => q{ };

my $config_file;

Getopt::Long::Configure('bundling');
unless (
    Getopt::Long::GetOptions(
        'c|config=s' => \$config_file,
        'h|help'     => sub { usage(); exit; },
    )
  )
{
    usage();
    exit 1;
}

die encode_utf8('Please use -h for usage information.')
  if @ARGV > 0;

die encode_utf8('No config file')
  unless length $config_file && -e $config_file;

my $yaml = LoadFile($config_file);

my $dbconnectstring = $yaml->{database};
die encode_utf8('No database connect string')
  unless length $dbconnectstring;

my $database = DBI->connect( 'dbi:Pg:' . $dbconnectstring,
    $EMPTY, $EMPTY, { AutoCommit => 0 } );

my $qa_list_sql = <<END_OF_QUERY;
SELECT
    h.source_name,
    h.source_version,
    h.start_time,
    h.lintian_version,
    h.release,
    h.port,
    COUNT(
        CASE WHEN (t.visibility = 'error'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS errors,
    COUNT(
        CASE WHEN (t.visibility = 'warning'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS warnings,
    COUNT(
        CASE WHEN (t.visibility = 'info'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS info,
    COUNT(
        CASE WHEN (t.visibility = 'pedantic'
            AND t.experimental = FALSE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS pedantic,
    COUNT(
        CASE WHEN (t.experimental = TRUE
            AND h.override = FALSE) THEN
            1
        ELSE
            NULL
        END) AS experimental,
    COUNT(
        CASE WHEN (h.override = TRUE) THEN
            1
        ELSE
            NULL
        END) AS overrides
FROM
    lintian.hints AS h
    JOIN lintian.tags AS t ON t.tag_name = h.tag_name
        AND t.lintian_version = h.lintian_version
WHERE (h.source_name, h.source_version, h.lintian_version, h.start_time, h.release, h.port) = (
    SELECT
        r2.source_name,
        r2.source_version,
        r2.lintian_version,
        r2.start_time,
        r2.release,
        r2.port
    FROM
        lintian.runs AS r2
    WHERE
        r2.source_name = h.source_name
    ORDER BY
        r2.source_name DESC,
        r2.source_version DESC,
        r2.lintian_version DESC,
        r2.start_time DESC,
        r2.release,
        r2.port
    LIMIT 1)
GROUP BY
    h.source_name,
    h.source_version,
    h.lintian_version,
    h.start_time,
    h.release,
    h.port
ORDER BY
    h.source_name,
    h.source_version,
    h.lintian_version,
    h.start_time,
    h.release,
    h.port
END_OF_QUERY

my $qa_list_sth = $database->prepare($qa_list_sql);
$qa_list_sth->execute;

while ( my $row = $qa_list_sth->fetchrow_hashref ) {

    my @order =
      qw{source_name errors warnings info pedantic experimental overrides};
    my @values = map { $row->{$_} } @order;

    my $source_line = join( $SPACE, @values );

    say encode_utf8($source_line);
}

$database->disconnect;

exit;

sub usage {
    print <<"END";
Usage: $0 -c CONFIG-FILE

    --config, -c   Use the named config file (required)
END
    return;
}

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
