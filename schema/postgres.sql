--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Debian 13.5-0+deb11u1)
-- Dumped by pg_dump version 13.5 (Debian 13.5-0+deb11u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: archive; Type: SCHEMA; Schema: -; Owner: lechner
--

CREATE SCHEMA archive;


ALTER SCHEMA archive OWNER TO lechner;

--
-- Name: lintian; Type: SCHEMA; Schema: -; Owner: lechner
--

CREATE SCHEMA lintian;


ALTER SCHEMA lintian OWNER TO lechner;

--
-- Name: pg_repack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_repack WITH SCHEMA public;


--
-- Name: EXTENSION pg_repack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_repack IS 'Reorganize tables in PostgreSQL databases with minimal locks';


--
-- Name: scheduler; Type: SCHEMA; Schema: -; Owner: lechner
--

CREATE SCHEMA scheduler;


ALTER SCHEMA scheduler OWNER TO lechner;

--
-- Name: debversion; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS debversion WITH SCHEMA public;


--
-- Name: EXTENSION debversion; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION debversion IS 'Debian version number data type';


--
-- Name: pgstattuple; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgstattuple WITH SCHEMA public;


--
-- Name: EXTENSION pgstattuple; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgstattuple IS 'show tuple-level statistics';


--
-- Name: semver; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS semver WITH SCHEMA public;


--
-- Name: EXTENSION semver; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION semver IS 'Semantic version data type';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: distribution_liberty; Type: TYPE; Schema: public; Owner: lechner
--

CREATE TYPE public.distribution_liberty AS ENUM (
    'main',
    'contrib',
    'non-free'
);


ALTER TYPE public.distribution_liberty OWNER TO lechner;

--
-- Name: enum_severity; Type: TYPE; Schema: public; Owner: lechner
--

CREATE TYPE public.enum_severity AS ENUM (
    'error',
    'warning',
    'info',
    'pedantic',
    'classification'
);


ALTER TYPE public.enum_severity OWNER TO lechner;

--
-- Name: index; Type: TYPE; Schema: public; Owner: lechner
--

CREATE TYPE public.index AS ENUM (
    'none',
    'orig',
    'patched',
    'installed',
    'control'
);


ALTER TYPE public.index OWNER TO lechner;

--
-- Name: installable_architecture; Type: TYPE; Schema: public; Owner: lechner
--

CREATE TYPE public.installable_architecture AS ENUM (
    'all',
    'amd64',
    'arm64',
    'armel',
    'armhf',
    'i386',
    'mipsel',
    'mips64el',
    'ppc64el',
    's390x'
);


ALTER TYPE public.installable_architecture OWNER TO lechner;

--
-- Name: port; Type: TYPE; Schema: public; Owner: lechner
--

CREATE TYPE public.port AS ENUM (
    'amd64',
    'arm64',
    'armel',
    'armhf',
    'i386',
    'mipsel',
    'mips64el',
    'ppc64el',
    's390x'
);


ALTER TYPE public.port OWNER TO lechner;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: autoreject_tags; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.autoreject_tags (
    tag_name text NOT NULL,
    mandatory boolean NOT NULL
);


ALTER TABLE archive.autoreject_tags OWNER TO lechner;

--
-- Name: debian_tarballs; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.debian_tarballs (
    basename text NOT NULL,
    archive_component public.distribution_liberty NOT NULL,
    source_name text NOT NULL,
    source_version public.debversion NOT NULL,
    size bigint NOT NULL
);


ALTER TABLE archive.debian_tarballs OWNER TO lechner;

--
-- Name: distributors; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.distributors (
    mailbox text NOT NULL,
    source_name text NOT NULL,
    source_version public.debversion NOT NULL
);


ALTER TABLE archive.distributors OWNER TO lechner;

--
-- Name: installables; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.installables (
    source_name text NOT NULL,
    installable_liberty public.distribution_liberty NOT NULL,
    source_version public.debversion NOT NULL,
    installable_version public.debversion NOT NULL,
    size bigint NOT NULL,
    description text NOT NULL,
    installed_size_k integer NOT NULL,
    installable_name text NOT NULL,
    installable_architecture public.installable_architecture NOT NULL,
    pool_path text NOT NULL
);


ALTER TABLE archive.installables OWNER TO lechner;

--
-- Name: mailboxes; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.mailboxes (
    rfc5322 text NOT NULL,
    phrase text NOT NULL,
    address text NOT NULL,
    "user" text NOT NULL,
    host text NOT NULL
);


ALTER TABLE archive.mailboxes OWNER TO lechner;

--
-- Name: orig_tarballs; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.orig_tarballs (
    basename text NOT NULL,
    archive_component public.distribution_liberty NOT NULL,
    source_name text NOT NULL,
    upstream_version text NOT NULL,
    source_component text DEFAULT ''::text NOT NULL,
    size bigint NOT NULL
);


ALTER TABLE archive.orig_tarballs OWNER TO lechner;

--
-- Name: released_installables; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.released_installables (
    installable_name text NOT NULL,
    installable_version public.debversion NOT NULL,
    release text NOT NULL,
    port public.port NOT NULL,
    installable_architecture public.installable_architecture NOT NULL
);


ALTER TABLE archive.released_installables OWNER TO lechner;

--
-- Name: released_sources; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.released_sources (
    source_name text NOT NULL,
    release text NOT NULL,
    source_version public.debversion NOT NULL
);


ALTER TABLE archive.released_sources OWNER TO lechner;

--
-- Name: releases; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.releases (
    release_name text NOT NULL,
    channel text NOT NULL
);


ALTER TABLE archive.releases OWNER TO lechner;

--
-- Name: sources; Type: TABLE; Schema: archive; Owner: lechner
--

CREATE TABLE archive.sources (
    source_name text NOT NULL,
    source_version public.debversion NOT NULL,
    homepage text DEFAULT ''::text NOT NULL,
    vcs_browser text DEFAULT ''::text NOT NULL,
    source_liberty public.distribution_liberty NOT NULL,
    upstream_version text NOT NULL,
    pool_path text NOT NULL
);


ALTER TABLE archive.sources OWNER TO lechner;

--
-- Name: hints; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.hints (
    tag_name text NOT NULL,
    source_name text NOT NULL,
    start_time timestamp with time zone NOT NULL,
    override boolean DEFAULT false NOT NULL,
    "position" integer NOT NULL,
    source_version public.debversion NOT NULL,
    lintian_version public.semver NOT NULL,
    pool_path text NOT NULL,
    release text NOT NULL,
    port public.port NOT NULL,
    item_index public.index DEFAULT 'none'::public.index NOT NULL,
    item_name text DEFAULT ''::text NOT NULL,
    item_line_position integer DEFAULT 0 NOT NULL,
    override_justification text DEFAULT ''::text NOT NULL,
    note text DEFAULT ''::text NOT NULL
);


ALTER TABLE lintian.hints OWNER TO lechner;

--
-- Name: inputs; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.inputs (
    source_name text NOT NULL,
    source_version public.debversion NOT NULL,
    lintian_version public.semver NOT NULL,
    start_time timestamp with time zone NOT NULL,
    pool_path text NOT NULL,
    release text NOT NULL,
    port public.port NOT NULL
);


ALTER TABLE lintian.inputs OWNER TO lechner;

--
-- Name: manual_pages; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.manual_pages (
    lintian_version public.semver NOT NULL,
    path text NOT NULL,
    html text NOT NULL
);


ALTER TABLE lintian.manual_pages OWNER TO lechner;

--
-- Name: masks; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.masks (
    source_name text NOT NULL,
    source_version public.debversion NOT NULL,
    lintian_version public.semver NOT NULL,
    start_time timestamp with time zone NOT NULL,
    release text NOT NULL,
    port public.port NOT NULL,
    pool_path text NOT NULL,
    "position" integer NOT NULL,
    screen text NOT NULL,
    excuse text DEFAULT ''::text NOT NULL
);


ALTER TABLE lintian.masks OWNER TO lechner;

--
-- Name: renamed_tags; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.renamed_tags (
    renamed_from text NOT NULL,
    tag_name text NOT NULL,
    lintian_version public.semver NOT NULL
);


ALTER TABLE lintian.renamed_tags OWNER TO lechner;

--
-- Name: runs; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.runs (
    source_name text NOT NULL,
    start_time timestamp with time zone NOT NULL,
    end_time timestamp with time zone NOT NULL,
    messages text[] DEFAULT '{}'::text[] NOT NULL,
    source_version public.debversion NOT NULL,
    lintian_version public.semver NOT NULL,
    invocation_options text NOT NULL,
    exit_status integer NOT NULL,
    port public.port NOT NULL,
    release text NOT NULL
);


ALTER TABLE lintian.runs OWNER TO lechner;

--
-- Name: screens; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.screens (
    screen text NOT NULL,
    tag_name text NOT NULL,
    lintian_version public.semver NOT NULL,
    reason_html text NOT NULL,
    see_also_html text[] NOT NULL,
    advocates text[] NOT NULL
);


ALTER TABLE lintian.screens OWNER TO lechner;

--
-- Name: tags; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.tags (
    explanation_html text NOT NULL,
    visibility public.enum_severity NOT NULL,
    experimental boolean DEFAULT false NOT NULL,
    tag_name text NOT NULL,
    "check" text NOT NULL,
    name_spaced boolean DEFAULT false NOT NULL,
    show_always boolean DEFAULT false NOT NULL,
    see_also_html text[] DEFAULT '{}'::text[] NOT NULL,
    lintian_version public.semver NOT NULL
);


ALTER TABLE lintian.tags OWNER TO lechner;

--
-- Name: versions; Type: TABLE; Schema: lintian; Owner: lechner
--

CREATE TABLE lintian.versions (
    lintian_version public.semver NOT NULL,
    repository_reference text NOT NULL
);


ALTER TABLE lintian.versions OWNER TO lechner;

--
-- Name: autoreject_tags autoreject_tags_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.autoreject_tags
    ADD CONSTRAINT autoreject_tags_pkey PRIMARY KEY (tag_name);


--
-- Name: distributors distributors_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.distributors
    ADD CONSTRAINT distributors_pkey PRIMARY KEY (source_name, source_version, mailbox);


--
-- Name: installables installables_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.installables
    ADD CONSTRAINT installables_pkey PRIMARY KEY (installable_name, installable_version, installable_architecture);


--
-- Name: mailboxes mailboxes_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.mailboxes
    ADD CONSTRAINT mailboxes_pkey PRIMARY KEY (rfc5322);


--
-- Name: released_installables released_installables_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.released_installables
    ADD CONSTRAINT released_installables_pkey PRIMARY KEY (installable_name, installable_version, installable_architecture, release, port);


--
-- Name: released_sources source_suites_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.released_sources
    ADD CONSTRAINT source_suites_pkey PRIMARY KEY (source_name, source_version, release);


--
-- Name: sources sources_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (source_name, source_version);


--
-- Name: releases suites_pkey; Type: CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.releases
    ADD CONSTRAINT suites_pkey PRIMARY KEY (release_name);


--
-- Name: hints hints_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.hints
    ADD CONSTRAINT hints_pkey PRIMARY KEY (source_name, source_version, lintian_version, start_time, release, port, pool_path, "position");


--
-- Name: inputs inputs_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.inputs
    ADD CONSTRAINT inputs_pkey PRIMARY KEY (source_name, source_version, lintian_version, start_time, release, port, pool_path);


--
-- Name: manual_pages manual_pages_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.manual_pages
    ADD CONSTRAINT manual_pages_pkey PRIMARY KEY (path, lintian_version);


--
-- Name: masks masks_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.masks
    ADD CONSTRAINT masks_pkey PRIMARY KEY (source_name, source_version, lintian_version, start_time, release, port, pool_path, "position", screen);


--
-- Name: versions releases_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.versions
    ADD CONSTRAINT releases_pkey PRIMARY KEY (lintian_version);


--
-- Name: renamed_tags renamed_tags_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.renamed_tags
    ADD CONSTRAINT renamed_tags_pkey PRIMARY KEY (renamed_from, lintian_version);


--
-- Name: runs runs_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.runs
    ADD CONSTRAINT runs_pkey PRIMARY KEY (source_name, source_version, lintian_version, start_time, release, port);


--
-- Name: screens screens_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.screens
    ADD CONSTRAINT screens_pkey PRIMARY KEY (screen, lintian_version);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (tag_name, lintian_version);


--
-- Name: distributors_mailbox_idx; Type: INDEX; Schema: archive; Owner: lechner
--

CREATE INDEX distributors_mailbox_idx ON archive.distributors USING btree (mailbox);


--
-- Name: installables_pool_path_idx; Type: INDEX; Schema: archive; Owner: lechner
--

CREATE UNIQUE INDEX installables_pool_path_idx ON archive.installables USING btree (pool_path);


--
-- Name: mailboxes_email_idx; Type: INDEX; Schema: archive; Owner: lechner
--

CREATE INDEX mailboxes_email_idx ON archive.mailboxes USING btree (address);


--
-- Name: mailboxes_name_idx; Type: INDEX; Schema: archive; Owner: lechner
--

CREATE INDEX mailboxes_name_idx ON archive.mailboxes USING btree (phrase);


--
-- Name: sources_pool_path_idx; Type: INDEX; Schema: archive; Owner: lechner
--

CREATE UNIQUE INDEX sources_pool_path_idx ON archive.sources USING btree (pool_path);


--
-- Name: sources_source_name_source_version_archive_component_idx; Type: INDEX; Schema: archive; Owner: lechner
--

CREATE UNIQUE INDEX sources_source_name_source_version_archive_component_idx ON archive.sources USING btree (source_name, source_version, source_liberty);


--
-- Name: hints_tags_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX hints_tags_idx ON lintian.hints USING btree (tag_name, lintian_version);


--
-- Name: inputs_pool_path_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX inputs_pool_path_idx ON lintian.inputs USING btree (pool_path);


--
-- Name: renamed_tags_lintian_version_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX renamed_tags_lintian_version_idx ON lintian.renamed_tags USING btree (lintian_version);


--
-- Name: renamed_tags_tag_name_lintian_version_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX renamed_tags_tag_name_lintian_version_idx ON lintian.renamed_tags USING btree (tag_name, lintian_version);


--
-- Name: runs_lintian_version_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX runs_lintian_version_idx ON lintian.runs USING btree (lintian_version);


--
-- Name: runs_released_sources_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX runs_released_sources_idx ON lintian.runs USING btree (source_name, source_version, release);


--
-- Name: screens_tags_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX screens_tags_idx ON lintian.screens USING btree (tag_name, lintian_version);


--
-- Name: tags_check_version_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX tags_check_version_idx ON lintian.tags USING btree ("check", lintian_version);


--
-- Name: tags_version_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX tags_version_idx ON lintian.tags USING btree (lintian_version);


--
-- Name: tags_visibility_version_idx; Type: INDEX; Schema: lintian; Owner: lechner
--

CREATE INDEX tags_visibility_version_idx ON lintian.tags USING btree (visibility, lintian_version);


--
-- Name: distributors distributors_mailbox_fkey; Type: FK CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.distributors
    ADD CONSTRAINT distributors_mailbox_fkey FOREIGN KEY (mailbox) REFERENCES archive.mailboxes(rfc5322);


--
-- Name: released_installables installables_fkey; Type: FK CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.released_installables
    ADD CONSTRAINT installables_fkey FOREIGN KEY (installable_name, installable_version, installable_architecture) REFERENCES archive.installables(installable_name, installable_version, installable_architecture) ON DELETE CASCADE;


--
-- Name: released_sources released_sources_release_fkey; Type: FK CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.released_sources
    ADD CONSTRAINT released_sources_release_fkey FOREIGN KEY (release) REFERENCES archive.releases(release_name) ON DELETE CASCADE;


--
-- Name: distributors sources_fkey; Type: FK CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.distributors
    ADD CONSTRAINT sources_fkey FOREIGN KEY (source_name, source_version) REFERENCES archive.sources(source_name, source_version) ON DELETE CASCADE;


--
-- Name: released_sources sources_fkey; Type: FK CONSTRAINT; Schema: archive; Owner: lechner
--

ALTER TABLE ONLY archive.released_sources
    ADD CONSTRAINT sources_fkey FOREIGN KEY (source_name, source_version) REFERENCES archive.sources(source_name, source_version) ON DELETE CASCADE DEFERRABLE;


--
-- Name: hints hints_inputs_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.hints
    ADD CONSTRAINT hints_inputs_fkey FOREIGN KEY (source_name, source_version, lintian_version, start_time, release, port, pool_path) REFERENCES lintian.inputs(source_name, source_version, lintian_version, start_time, release, port, pool_path) ON DELETE CASCADE;


--
-- Name: hints hints_tags_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.hints
    ADD CONSTRAINT hints_tags_fkey FOREIGN KEY (tag_name, lintian_version) REFERENCES lintian.tags(tag_name, lintian_version) ON DELETE CASCADE;


--
-- Name: inputs inputs_runs_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.inputs
    ADD CONSTRAINT inputs_runs_fkey FOREIGN KEY (source_name, source_version, lintian_version, start_time, release, port) REFERENCES lintian.runs(source_name, source_version, lintian_version, start_time, release, port) ON DELETE CASCADE;


--
-- Name: manual_pages manual_pages_lintian_version_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.manual_pages
    ADD CONSTRAINT manual_pages_lintian_version_fkey FOREIGN KEY (lintian_version) REFERENCES lintian.versions(lintian_version) ON DELETE CASCADE;


--
-- Name: masks masks_hints_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.masks
    ADD CONSTRAINT masks_hints_fkey FOREIGN KEY (source_name, source_version, lintian_version, start_time, release, port, pool_path, "position") REFERENCES lintian.hints(source_name, source_version, lintian_version, start_time, release, port, pool_path, "position") ON DELETE CASCADE;


--
-- Name: masks masks_screens_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.masks
    ADD CONSTRAINT masks_screens_fkey FOREIGN KEY (screen, lintian_version) REFERENCES lintian.screens(screen, lintian_version) ON DELETE CASCADE;


--
-- Name: runs runs_released_sources_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.runs
    ADD CONSTRAINT runs_released_sources_fkey FOREIGN KEY (source_name, source_version, release) REFERENCES archive.released_sources(source_name, source_version, release) ON DELETE CASCADE DEFERRABLE;


--
-- Name: runs runs_versions_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.runs
    ADD CONSTRAINT runs_versions_fkey FOREIGN KEY (lintian_version) REFERENCES lintian.versions(lintian_version) ON DELETE CASCADE;


--
-- Name: screens screens_tags_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.screens
    ADD CONSTRAINT screens_tags_fkey FOREIGN KEY (tag_name, lintian_version) REFERENCES lintian.tags(tag_name, lintian_version) ON DELETE CASCADE;


--
-- Name: renamed_tags tags_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.renamed_tags
    ADD CONSTRAINT tags_fkey FOREIGN KEY (tag_name, lintian_version) REFERENCES lintian.tags(tag_name, lintian_version) ON DELETE CASCADE;


--
-- Name: tags tags_versions_fkey; Type: FK CONSTRAINT; Schema: lintian; Owner: lechner
--

ALTER TABLE ONLY lintian.tags
    ADD CONSTRAINT tags_versions_fkey FOREIGN KEY (lintian_version) REFERENCES lintian.versions(lintian_version) ON DELETE CASCADE;


--
-- Name: SCHEMA archive; Type: ACL; Schema: -; Owner: lechner
--

GRANT USAGE ON SCHEMA archive TO runner;
GRANT USAGE ON SCHEMA archive TO website;


--
-- Name: SCHEMA lintian; Type: ACL; Schema: -; Owner: lechner
--

GRANT USAGE ON SCHEMA lintian TO runner;
GRANT USAGE ON SCHEMA lintian TO website;


--
-- Name: SCHEMA scheduler; Type: ACL; Schema: -; Owner: lechner
--

GRANT USAGE ON SCHEMA scheduler TO website;
GRANT USAGE ON SCHEMA scheduler TO runner;


--
-- Name: TABLE autoreject_tags; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT ON TABLE archive.autoreject_tags TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.autoreject_tags TO runner;


--
-- Name: TABLE debian_tarballs; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.debian_tarballs TO runner;


--
-- Name: TABLE distributors; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT ON TABLE archive.distributors TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.distributors TO runner;


--
-- Name: TABLE installables; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.installables TO runner;


--
-- Name: TABLE mailboxes; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT ON TABLE archive.mailboxes TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.mailboxes TO runner;


--
-- Name: TABLE orig_tarballs; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.orig_tarballs TO runner;


--
-- Name: TABLE released_installables; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE archive.released_installables TO runner;


--
-- Name: TABLE released_sources; Type: ACL; Schema: archive; Owner: lechner
--

GRANT ALL ON TABLE archive.released_sources TO runner;
GRANT SELECT ON TABLE archive.released_sources TO website;


--
-- Name: TABLE releases; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT ON TABLE archive.releases TO website;
GRANT ALL ON TABLE archive.releases TO runner;


--
-- Name: TABLE sources; Type: ACL; Schema: archive; Owner: lechner
--

GRANT SELECT ON TABLE archive.sources TO website;
GRANT ALL ON TABLE archive.sources TO runner;


--
-- Name: TABLE hints; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT ON TABLE lintian.hints TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.hints TO runner;


--
-- Name: TABLE inputs; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT ON TABLE lintian.inputs TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.inputs TO runner;


--
-- Name: TABLE manual_pages; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT ON TABLE lintian.manual_pages TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.manual_pages TO runner;


--
-- Name: TABLE masks; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.masks TO runner;
GRANT SELECT ON TABLE lintian.masks TO website;


--
-- Name: TABLE renamed_tags; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.renamed_tags TO runner;
GRANT SELECT ON TABLE lintian.renamed_tags TO website;


--
-- Name: TABLE runs; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT ON TABLE lintian.runs TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.runs TO runner;


--
-- Name: TABLE screens; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.screens TO runner;
GRANT SELECT ON TABLE lintian.screens TO website;


--
-- Name: TABLE tags; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT ON TABLE lintian.tags TO website;
GRANT ALL ON TABLE lintian.tags TO runner;


--
-- Name: TABLE versions; Type: ACL; Schema: lintian; Owner: lechner
--

GRANT SELECT ON TABLE lintian.versions TO website;
GRANT SELECT,INSERT,DELETE,TRUNCATE,UPDATE ON TABLE lintian.versions TO runner;


--
-- PostgreSQL database dump complete
--

